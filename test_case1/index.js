// <![CDATA[
/* 
 * creates the greCaptcha render
 */
var onloadCallback = function () {
    grecaptcha.render('captcha', {
        'sitekey': '6Ld1DaIUAAAAAC7HiEQDeo7q6lWYzPH99cOLxE_t',
        'theme': 'light'
    });
};

/* Checks the fields of the form to make sure they're ready to be submitted
 * and that all the requried fields are there.
 */
function check_webtolead_fields() {
    if (document.getElementById('bool_id') != null) {
        var reqs = document.getElementById('bool_id').value;
        bools = reqs.substring(0, reqs.lastIndexOf(';'));
        var bool_fields = new Array();
        var bool_fields = bools.split(';');
        nbr_fields = bool_fields.length;
        for (var i = 0; i < nbr_fields; i++) {
            if (document.getElementById(bool_fields[i]).value == 'on') {
                document.getElementById(bool_fields[i]).value = 1;
            } else {
                document.getElementById(bool_fields[i]).value = 0;
            }
        }
    }
    if (document.getElementById('req_id') != null) {;
        var reqs = document.getElementById('req_id').value;
        reqs = reqs.substring(0, reqs.lastIndexOf(';'));
        var req_fields = new Array();
        var req_fields = reqs.split(';');
        nbr_fields = req_fields.length;
        var req = true;
        for (var i = 0; i < nbr_fields; i++) {
            if (document.getElementById(req_fields[i]).value.length <= 0 || document.getElementById(
                    req_fields[i]).value == 0) {
                req = false;
                break;
            }
        }
        if (req && grecaptcha.getResponse()!= "") {
            return true;
        } else {
            alert('Please provide all the required fields');
            return false;
        }
        return false
    } 
}
/* 
 * Checks to see whether or not the email is valid
 */
function validateEmailAdd() {
    if (document.getElementById('email') && document.getElementById('email').value.length > 0) {
        if (!document.getElementById('email').value.match(/^\w+(['\.\-\+]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,})+$/)) {
            document.getElementById('email').value = "";
            alert('Not a valid email address');
        }
    }
}
/* 
 * Checks to see whether or not the phone number is 10 digits and then reformats the phone number to be in
 * xxx-xxx-xxxx format.
 */
function formatPhoneNum() {
    if (document.getElementById('phone_work').value.length != 10) {
        alert("Please enter a 10 digit Phone Number ");
        document.getElementById('phone_work').value = "";
    } else if (!document.getElementById('phone_work').value.match(/[0-9]{3}[-][0-9]{3}[-][0-9]{4}/)) {
        var toBeReplaced = document.getElementById('phone_work').value;
        tobeReplaced = toBeReplaced.replace(/[^0-9]/gi, '');
        console.log(toBeReplaced);
        document.getElementById('phone_work').value = tobeReplaced.substring(0, 3) + "-" + tobeReplaced.substring(3, 6) + "-" + tobeReplaced.substring(6, 10);
    } else {
        console.log("Already Formatted");
    }
}
// ]]>