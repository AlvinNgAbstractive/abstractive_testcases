
/* 
 * Sends the information from the form to sugar using ajax after the submit button is pressed 
 */
$(document).ready(function () {
    $("#WebToLeadForm").submit(function (event) {
        if (grecaptcha.getResponse() != "" && check_webtolead_fields) {   
            console.log(grecaptcha.getResponse());
            event.preventDefault();
            var post_url = $(this).attr("action"); //get url
            var request_method = $(this).attr("method"); //get form GET/POST method
            var form_data = $(this).serialize(); //Serialize form elements

            $.ajax({
                url: post_url,
                type: 'POST',
                data: form_data,
                success: function () {
                    console.log("success!");
                },
                complete: function (event) {
                    $("#thanks").show();
                    $(".overlay").show();
                    $("input").val("");
                    $("#formSubmit").val("SUBMIT");
                }
            })
        } else {
            event.preventDefault();
        }
    });
    // closes the modal when the close button is pressed.
    $(".closeModal").click(function () {
        $("#thanks").hide();
        $(".overlay").hide();
    });
});