<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '|yR|zzaXFU]u(=78EE;`62ij1,Rp)XYJ7-YZa0yBq<Mc0%!Bi*cL5U!|S1Xi@Wi=' );
define( 'SECURE_AUTH_KEY',  'CyG;9R;pq:S.=nhGSm)(zLgQ<|k1[14P.9TXyJtya5+|&Q2=0hm+j:?XQ^TSAkD6' );
define( 'LOGGED_IN_KEY',    'z@KSR`![e%BqM,}bolR;%QavqJKRleMy9&cOo.{P9u F7y=f6I;o/=-ff)K|F 6b' );
define( 'NONCE_KEY',        ']G?mLNbFG~#ycbb3m<l_FH4Su[]j#1[#=|6w6qk7$bs;LH1H_{B<&fa,qDrdz~nt' );
define( 'AUTH_SALT',        'F*Fb0$JtU|&z+h{Ju@%$F<iU;-w56^ap^LAqZR!88Wd;{/^$?~vyL<!=Kc=jN 3g' );
define( 'SECURE_AUTH_SALT', 'F]TYxYDSys_0< WN_:]=4^C.TI/1<c6k*;yUGx8OiWm?d$;G~;t`pgL0aK_c?KF[' );
define( 'LOGGED_IN_SALT',   'DhXXoZwM3<&:^uKms&*@2cl9@RNx.vt y[j6Kw/*,i[J)*Yr{*5dCf$~@BT/QQmN' );
define( 'NONCE_SALT',       '6|B&2jIDw:iglfh>76K:t`qWcRm-LIkhL:Eo4@nb36gQ8}BR2yL$,sC@q0c-w3.8' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
