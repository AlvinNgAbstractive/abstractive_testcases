<?php
/* VARIABLES TO EDIT */
$url = "http://dev1.oaprojects.ca";
$username = "admin";
$password = "Welcome2019!";
// fields
$nameID = 'Field1';
$emailID = 'Field7';
$start_dateID = 'Field3';
$end_dateID = 'Field4';
$typeID = 'Field5';
$descriptionID = 'Field6';

/* To be set by Abstractive */
$module_name = "WEB_Vacations";
$url = $url . "/service/v4_1/rest.php";
$login_parameters = array(
     "user_auth" => array(
          "user_name" => $username,
          "password" => md5($password),
          "version" => "1"
     ),
     "application_name" => "vacation",
     "name_value_list" => array(),
);


$login_result = call("login", $login_parameters, $url);


//get session id
$session_id = $login_result->id;

if (checkEmpty($nameID, $emailID, $start_dateID, $end_dateID, $typeID) == true) {
     $response_array['status'] = 'error';
     printValues($nameID, $emailID, $start_dateID, $end_dateID, $typeID, $descriptionID);
     echo ("fail");
     return 1;
} else {
     printValues($nameID, $emailID, $start_dateID, $end_dateID, $typeID, $descriptionID);
     echo ("success");
     //create account -------------------------------------     
     $set_entry_parameters = array(
          //session id
          "session" => $session_id,

          //The name of the module from which to retrieve records.
          "module_name" => $module_name,

          //Record attributes
          "name_value_list" => array(
               //to update a record, you will need to pass in a record id as commented below
               array("name" => "name", "value" => $_POST[$nameID]),
               array("name" => "requester_email", "value" => $_POST[$emailID]),
               array("name" => "start_date", "value" => $_POST[$start_dateID] . "-" . $_POST[$start_dateID . "-1"] . "-" . $_POST[$start_dateID . "-2"]),
               array("name" => "end_date", "value" => $_POST[$end_dateID] . "-" . $_POST[$end_dateID . "-1"] . "-" . $_POST[$end_dateID . "-2"]),
               array("name" => "timeoff_type", "value" => $_POST[$typeID]),
               array("name" => "description", "value" => $_POST[$descriptionID]),
          ),
     );
     $set_entry_result = call("set_entry", $set_entry_parameters, $url);
}
function call($method, $parameters, $url)
{
     ob_start();
     $curl_request = curl_init();

     curl_setopt($curl_request, CURLOPT_URL, $url);
     curl_setopt($curl_request, CURLOPT_POST, 1);
     curl_setopt($curl_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
     curl_setopt($curl_request, CURLOPT_HEADER, 1);
     curl_setopt($curl_request, CURLOPT_SSL_VERIFYPEER, 0);
     curl_setopt($curl_request, CURLOPT_RETURNTRANSFER, 1);
     curl_setopt($curl_request, CURLOPT_FOLLOWLOCATION, 0);

     $jsonEncodedData = json_encode($parameters);

     $post = array(
          "method" => $method,
          "input_type" => "JSON",
          "response_type" => "JSON",
          "rest_data" => $jsonEncodedData
     );

     curl_setopt($curl_request, CURLOPT_POSTFIELDS, $post);
     $result = curl_exec($curl_request);
     curl_close($curl_request);

     $result = explode("\r\n\r\n", $result, 2);
     $response = json_decode($result[1]);
     ob_end_flush();

     return $response;
}
/*
* Checks if requred values are empty
* @params ID's used to get values from $_POST
* @return True if empty False if !empty
*/
function checkEmpty($nameID, $emailID, $start_dateID, $end_dateID, $typeID)
{
     if ($_POST[$nameID] == "") {
          error_log("empty nameID");
          return true;
     }
     if ($_POST[$emailID] == "") {
          error_log("empty emailID");
          return true;
     }
     if ($_POST[$start_dateID] == "") {
          error_log("empty start_dateID");
          return true;
     }
     if ($_POST[$end_dateID] == "") {
          error_log("empty end_dateID");
          return true;
     }
     if ($_POST[$typeID] == "") {
          error_log("empty typeID");
          return true;
     }
     return false;
}

/* Prints values for debugging */
function printValues($nameID, $emailID, $start_dateID, $end_dateID, $typeID, $descriptionID)
{
     echo ("name: " . $_POST[$nameID]);
     echo ("start_date: " .  $_POST[$start_dateID] . "-" . $_POST[$start_dateID . "-1"] . "-" . $_POST[$start_dateID . "-2"]);
     echo ("end_dateID: " .  $_POST[$end_dateID] . "-" . $_POST[$end_dateID . "-1"] . "-" . $_POST[$end_dateID . "-2"]);
     echo ("descriptionID: " . $_POST[$descriptionID]);
     echo ("emailID: " .  $_POST[$emailID]);
     echo ("typeID: " . $_POST[$typeID]);
}
