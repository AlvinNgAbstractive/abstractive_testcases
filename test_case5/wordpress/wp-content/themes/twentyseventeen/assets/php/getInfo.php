<?php
/* Gets the account information of the user so we can autofill it into the wordpress form for vacations and future forms as well */
include "../../../../../wp-load.php";

$current_user = wp_get_current_user();

$json['user_email'] = $current_user->user_email;
$json['user_name'] = $current_user->display_name;

echo json_encode($json);
