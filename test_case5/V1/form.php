<form id="form5" class="wufoo topLabel page" accept-charset="UTF-8" action="https://alvinng1998.wufoo.com/forms/skh4kby1f1a4r4/#public" autocomplete="off" enctype="multipart/form-data" method="post" name="form5" novalidate="">
    <header id="header" class="info">
        <h2 class="leftFormProp">Vacations</h2>
        <div class="0"></div>
    </header>
    <section>
        <h3 id="title24">Please enter your full name</h3>
    </section><label id="title1" class="desc" for="Field1">
        Full Name <span id="req_1" class="req">*</span></label>
    <div><input id="Field1" class="field text medium" tabindex="0" maxlength="255" name="Field1" type="text" value="" placeholder="Enter Name Here" required=""></div>
    <section>
        <h3 id="title23">Dates</h3>
        <div id="instruct23">When would you like your vacation to be?</div>
    </section><span id="foli135" class="date notranslate      ">
        <label id="title135" class="desc" for="Field135">
            Start Date: <span id="req_135" class="req">*</span>
        </label>
        <label for="Field135-1"> MM</label>
        <input id="Field135-1" class="field text" tabindex="0" max="12" maxlength="2" min="01" name="Field135-1" size="2" type="number" value="1" required="">
        <label for="Field135-2"> DD</label>
        <input id="Field135-2" class="field text" tabindex="0" max="31" maxlength="2" min="01" name="Field135-2" size="2" type="number" value="1" required="">
        <label for="Field135"> YYYY</label>
        <input id="Field135" class="field text" tabindex="0" maxlength="4" name="Field135" required="" size="4" type="number" value="2019"></span>

    <span id="foli136" class="date notranslate">
        <label id="title136" class="desc" for="Field136">
            End Date: <span id="req_136" class="req">*</span>
        </label>
        <label for="Field136-1"> MM</label>
        <input id="Field136-1" class="field text" tabindex="0" max="12" maxlength="2" min="01" name="Field136-1" size="2" type="number" value="1" required=""></span>

    <label for="Field136-2"> DD</label>
    <input id="Field136-2" class="field text" tabindex="0" max="31" maxlength="2" min="01" name="Field136-2" size="2" type="number" value="1" required="">

    <label for="Field136"> YYYY</label>
    <input id="Field136" class="field text" tabindex="0" maxlength="4" name="Field136" size="4" type="number" value="2019" required=""><label id="title126" class="desc" for="Field126"></label>
    <label id="title126" class="desc" for="Field126"></label>

    <label id="title126" class="desc" for="Field126">Any particular reason you need these dates?
    </label>
    <div><textarea id="Field126" class="field textarea medium" tabindex="0" spellcheck="true" cols="50" name="Field126" rows="10" placeholder="(optional)"></textarea>

    </div>
    <ul>
        <li class="buttons ">
            <div><input id="saveForm" class="btTxt submit" name="saveForm" type="submit" value="Submit"></div>
        </li>
        <li class="hide"><label for="comment">Do Not Fill This Out</label>
            <textarea id="comment" cols="1" name="comment" rows="1"></textarea>
            <input id="idstamp" name="idstamp" type="hidden" value="wI9h2CE1duk41asL0jqUTqrT4Z+cirLDcdkZKXewqVU="></li>
    </ul>
</form>