<?php

    //get the posted JSON data
    $data = file_get_contents('php://input');
    //decode the data
    $decoded_data = json_decode(trim($data));

    //use the data
    $file = 'receivedData-'.time().'.txt';
    file_put_contents($file, print_r($decoded_data, true));

?>
