<?php
/* VARIABLES TO EDIT */
$url = "http://dev1.oaprojects.ca";
$username = "admin";
$password = "Welcome2019!";
$nameID = 'Field1';
$start_dateID = 'Field135';
$end_dateID = 'Field136';
$descriptionID = 'Field126';
$newFieldID ='Field138';
$module_name = "vaca_vacation";

/* To be set by Abstractive */
$url = $url."/service/v4_1/rest.php";
$login_parameters = array(
     "user_auth" => array(
          "user_name" => $username,
          "password" => md5($password),
          "version" => "1"
     ),
     "application_name" => "applyVacation",
     "name_value_list" => array(),
);

$login_result = call("login", $login_parameters, $url);


//get session id
$session_id = $login_result->id;

//create account -------------------------------------     
$set_entry_parameters = array(
     //session id
     "session" => $session_id,

     //The name of the module from which to retrieve records.
     "module_name" => $module_name,

     //Record attributes
     "name_value_list" => array(
          //to update a record, you will need to pass in a record id as commented below
          array("name" => "name", "value" => $_POST[$nameID]),
          array("name" => "start_date", "value" => ($_POST[($start_dateID . "-1")] . "-" . $_POST[($start_dateID . "-2")]  . "-" . $_POST[$start_dateID])),
          array("name" => "end_date", "value" => ($_POST[$end_dateID . '-1'] . "-" . $_POST[$end_dateID . '-2'] . "-" . $_POST[$end_dateID])),
          array("name" => "description", "value" => $_POST[$descriptionID]),
          array("name" => "newfield_c", "value" => $_POST[$newFieldID]),
     ),
);

$set_entry_result = call("set_entry", $set_entry_parameters, $url);

function call($method, $parameters, $url)
{
     ob_start();
     $curl_request = curl_init();

     curl_setopt($curl_request, CURLOPT_URL, $url);
     curl_setopt($curl_request, CURLOPT_POST, 1);
     curl_setopt($curl_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
     curl_setopt($curl_request, CURLOPT_HEADER, 1);
     curl_setopt($curl_request, CURLOPT_SSL_VERIFYPEER, 0);
     curl_setopt($curl_request, CURLOPT_RETURNTRANSFER, 1);
     curl_setopt($curl_request, CURLOPT_FOLLOWLOCATION, 0);

     $jsonEncodedData = json_encode($parameters);

     $post = array(
          "method" => $method,
          "input_type" => "JSON",
          "response_type" => "JSON",
          "rest_data" => $jsonEncodedData
     );

     curl_setopt($curl_request, CURLOPT_POSTFIELDS, $post);
     $result = curl_exec($curl_request);
     curl_close($curl_request);

     $result = explode("\r\n\r\n", $result, 2);
     $response = json_decode($result[1]);
     ob_end_flush();

     return $response;
}
