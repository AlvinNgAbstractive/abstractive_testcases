pre-req:
    - customer has some sort of company website (it could be wordpress etc..)

    - they are thinking to use some sort of form generating plugin to generate form (we are looking at the wufoo form generating plugin create form page on their website)

    - they are gonna create a vacation (time off) submission form page

    - when they submit the vacation (timeoff) form, we would like to have that to populate on sugarcrm 
      (take a look at wufoo form and generate vacation template)



  sugarcrm:
    - need to create a custom module: Time Off or Vacations whatever you want

    - type: basic

    - fields: Start Date, End Date

    - when you create the basic type module it comes with base fields: id, name, date_modified, date_entered, description, etc..


todo 
- add in date checking to make sure it's an actual date
- styling