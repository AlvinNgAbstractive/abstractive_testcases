/* Edit Variables here */
var formID = "#form2"; //Form ID
var NameID = "#Field8"; //Field ID for Name
// var lastNameID = "#Field9"; //Field ID for Name
var typeID = "#Field6";
var startDateID = "#Field2";

var phpURL = "http://dev1-wordpress.abstractiveapps.ca/wp-content/themes/twentyseventeen/assets/php/SubmitEntries.php"; //URL of SubmitEntries.php

$(document).ready(function () {
    $(formID).on("submit", function (event) {
        event.preventDefault();
        var today = new Date();
        var tempF1 = $(NameID).val(); //add start_Date, and type to username field
        if (tempF1 != "")
            // $(NameID).val(tempF1 + " \n[" + today.getMonth() + '/' + today.getDate() + '/' + today.getFullYear() + "@" + today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds() + "]");
            $(NameID).val(tempF1 + " | " + $(typeID).val() + " | " + $(startDateID).val() + "-" + $(startDateID + "-1").val() + "-" + $(startDateID + "-2").val())
        var post_url = $(this).attr("action");
        var form_data = $(this).serialize();

        $.ajax({
            url: post_url,
            type: 'POST',
            data: form_data,
            complete: function (event) {
                console.log(event);
                if (event.readyState == 0 && ($('#Field6').val() != "")) {
                    $.ajax({
                        url: phpURL,
                        type: 'POST',
                        data: form_data,
                        success: function () {
                            alert("Your form has been submitted!");
                            window.location.reload();
                        },
                        complete: function (event) {
                            console.log("Sugar complete!");
                        }
                    });
                } else {
                    alert("Sorry, One or more of your fields have been entered incorrectly.");
                    window.location.reload();
                }
            },
        });


    });
});