<?php
/* VARIABLES TO EDIT */
$url = "http://dev1-demo.abstractiveapps.ca";
$username = "SugarCRMUpgrade";
$password = "Welcome2019!";

/* Fields */
$nameID = 'Field8';
$emailID = 'Field1';
$start_dateID = 'Field2';
$end_dateID = 'Field3';
$typeID = 'Field6';
$descriptionID = 'Field10';

/* Login */
$module_name = "WEB_Vacations";
$url = $url . "/service/v4_1/rest.php";
$login_parameters = array(
     "user_auth" => array(
          "user_name" => $username,
          "password" => md5($password),
          "version" => "1"
     ),
     "application_name" => "applyVacation",
     "name_value_list" => array(),
);

/* get session id */
$login_result = call("login", $login_parameters, $url);
$session_id = $login_result->id;

if (checkEmpty($nameID, $emailID, $start_dateID, $end_dateID, $typeID) == true) {
     $response_array['status'] = 'error';
     return 1;
}
// printValues($nameID, $emailID, $start_dateID, $end_dateID, $typeID, $descriptionID);
//create account      
$set_entry_parameters = array(
     //session id
     "session" => $session_id,

     //The name of the module from which to retrieve records.
     "module_name" => $module_name,

     //Record attributes
     "name_value_list" => array(
          //to update a record, you will need to pass in a record id as commented below
          array("name" => "name", "value" => $_POST[$nameID]),
          array("name" => "start_date", "value" => $_POST[$start_dateID] . "-" . $_POST[$start_dateID . "-1"] . "-" . $_POST[$start_dateID . "-2"]),
          array("name" => "end_date", "value" => $_POST[$end_dateID] . "-" . $_POST[$end_dateID . "-1"] . "-" . $_POST[$end_dateID . "-2"]),
          array("name" => "description", "value" => $_POST[$descriptionID]),
          array("name" => "requester_email", "value" => $_POST[$emailID]),
          array("name" => "timeoff_type", "value" => $_POST[$typeID]),
     ),
);


$set_entry_result = call("set_entry", $set_entry_parameters, $url);
$response_array['status'] = 'success';
return 0;
function call($method, $parameters, $url)
{
     ob_start();
     $curl_request = curl_init();

     curl_setopt($curl_request, CURLOPT_URL, $url);
     curl_setopt($curl_request, CURLOPT_POST, 1);
     curl_setopt($curl_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
     curl_setopt($curl_request, CURLOPT_HEADER, 1);
     curl_setopt($curl_request, CURLOPT_SSL_VERIFYPEER, 0);
     curl_setopt($curl_request, CURLOPT_RETURNTRANSFER, 1);
     curl_setopt($curl_request, CURLOPT_FOLLOWLOCATION, 0);

     $jsonEncodedData = json_encode($parameters);

     $post = array(
          "method" => $method,
          "input_type" => "JSON",
          "response_type" => "JSON",
          "rest_data" => $jsonEncodedData
     );

     curl_setopt($curl_request, CURLOPT_POSTFIELDS, $post);
     $result = curl_exec($curl_request);
     curl_close($curl_request);

     $result = explode("\r\n\r\n", $result, 2);
     $response = json_decode($result[1]);
     ob_end_flush();

     return $response;
}

function checkEmpty($nameID, $emailID, $start_dateID, $end_dateID, $typeID)
{
     if ($_POST[$nameID] == "") {
          error_log("empty nameID");
          return true;
     }
     if ($_POST[$emailID] == "") {
          error_log("empty emailID");
          return true;
     }
     if ($_POST[$start_dateID] == "") {
          error_log("empty start_dateID");
          return true;
     }
     if ($_POST[$end_dateID] == "") {
          error_log("empty end_dateID");
          return true;
     }
     if ($_POST[$typeID] == "") {
          error_log("empty typeID");
          return true;
     }
     return false;
}
/* for debugging */
function printValues($nameID, $emailID, $start_dateID, $end_dateID, $typeID, $descriptionID)
{
     error_log("name: " . $_POST[$nameID]);
     error_log("start_date: " .  $_POST[$start_dateID] . "-" . $_POST[$start_dateID . "-1"] . "-" . $_POST[$start_dateID . "-2"]);
     error_log("end_dateID: " .  $_POST[$end_dateID] . "-" . $_POST[$end_dateID . "-1"] . "-" . $_POST[$end_dateID . "-2"]);
     error_log("descriptionID: " . $_POST[$descriptionID]);
     error_log("emailID: " .  $_POST[$emailID]);
     error_log("typeID: " . $_POST[$typeID]);
}
