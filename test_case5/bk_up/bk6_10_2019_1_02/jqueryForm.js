/* Edit Variables here */
var formID = "#form1"; //Form ID
var nameID = "#Field1"; //Field ID for Name
var emailID = "#Field2"; //Field ID for Email
var startDateID = "#Field3"; //Field ID for Start
var endDateID = "#Field4"; //Field ID for End
var typeID = "#Field5"; //Field ID for Type

var phpURL = "http://wordpress.example.ca/wp-content/themes/twentyseventeen/assets/php/SubmitEntries.php"; //URL of SubmitEntries.php

$(document).ready(function () {
    $(formID).on("submit", function (event) {
        disableStartEnd();
        event.preventDefault();
        setName(nameID, typeID, startDateID);
        var post_url = $(this).attr("action");
        var form_data = $(this).serialize();

        $.ajax({
            url: post_url,
            type: 'POST',
            data: form_data,
            complete: function (event) {
                console.log(event);
                if (event.readyState == 0 && (!checkEmpty(nameID, emailID, startDateID, endDateID, typeID))) {
                    $.ajax({
                        url: phpURL,
                        type: 'POST',
                        data: form_data,
                        success: function () {
                            alert("Your form has been submitted!");
                            window.location.reload();
                        },
                        complete: function (event) {
                            console.log("Sugar complete!");
                        }
                    });
                } else {
                    alert("Sorry, One or more of your fields have been entered incorrectly.");
                    printFields();
                    window.location.reload();
                }
            },
        });


    });
});
/* splits the date onchange 
   @DateFormat data-date-format="yyyy-mm-dd"
   @param DateID the id of the date field
   @param DateFieldID the id of the wufoo date field to change value of
*/
function splitDate(dateID, dateFieldID) {
    var arr = $(dateID).val().split("-");

    $(dateFieldID).val(arr[0]); //year
    $(dateFieldID + "-1").val(arr[1]); //month
    $(dateFieldID + "-2").val(arr[2]); //date
}

/* Prints values for debugging */
function printFields() {
    console.log($(nameID).val());
    console.log($(emailID).val());
    console.log($(start_dateID).val());
    console.log($(end_dateID).val());
    console.log($(typeID).val());
}
/* 
 * Checks to see whether or not the email is valid
 */
function validateEmailAdd() {
    if ($(emailID).val() && $(emailID).val().length > 0) {
        if (!$(emailID).val().match(/^\w+(['\.\-\+]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,})+$/)) {
            $(emailID).val("");
            alert('Not a valid email address');
        }
    }
}
/*
 * Checks if requred values are empty
 * @params ID's used to get values from $().val()
 * @return True if empty False if !empty
 */
function checkEmpty(nameID, emailID, startDateID, endDateID, typeID) {
    if ($(nameID).val() == "") {
        alert("empty nameID");
        return true;
    }
    if ($(emailID).val() == "") {
        alert("empty emailID");
        return true;
    }
    if ($(startDateID).val() == "") {
        alert("empty start_dateID");
        return true;
    }
    if ($(endDateID).val() == "") {
        alert("empty end_dateID");
        return true;
    }
    if ($(typeID).val() == "") {
        alert("empty typeID");
        return true;
    }
    return false;
}

/* sets the name to Name|type|startDate for unique entries 
    @param nameID the fieldID for the name
    @param typeID the fieldID for the type
    @param startDateID the fieldID for the startDate
*/
function setName(nameID, typeID, startDateID) {
    var today = new Date();
    var tempF1 = $(nameID).val(); //add date to userfield
    if (tempF1 != "")
        // $(firstNameID).val(tempF1 + " \n[" + today.getMonth() + '/' + today.getDate() + '/' + today.getFullYear() + "@" + today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds() + "]");
        $(nameID).val(tempF1 + " | " + $(typeID).val() + " | " + $(startDateID).val() + "-" + $(startDateID + "-1").val() + "-" + $(startDateID + "-2").val())

}
/* Disables the startdate and end date
 */
function disableStartEnd() {
    $("#start_date").disabled = true;
    $("#end_date").disabled = true;
}