<!DOCTYPE html>
<html lang="en">

<?php session_start();    ?>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/skeleton.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="js/index.js"></script>
    <title>Data Extract</title>
</head>

<body>
    <label for="username">username: </label>
    <input type="text" id="username">
    <label for="password">password: </label>
    <input type="password" id="password">
    <button id="login">Login</button>
    <?php
    $_SESSION['iUrl'] = 'https://sg-angscrmdemo.demo.sugarcrm.com/rest/v11';
    $instance_url = $_SESSION['iUrl'];

    if (isset($_POST['username']) && isset($_POST['password'])) {
        $_SESSION['user'] = $_POST['username'];
        $_SESSION['pass'] = $_POST['password'];
        if (getToken($_SESSION['user'], $_SESSION['pass'], $instance_url)) {
            $_SESSION['token'] = getToken($_SESSION['user'], $_SESSION['pass'], $instance_url);
            $response = array();
            $response['login_status'] = 'success';
            echo json_encode('success');
        } else {
            $success = 'false';
        }
    } else {
        $_POST['username'] = null;
        $_SESSION['user'] = null;
        $_POST['password'] = null;
        $_SESSION['pass'] = null;
    }


    // username ='admin'
    // password ='Sugar123 !'


    // echo $_SESSION['token'];
    // echo "false";
    function getToken($username, $password, $instance_url)
    {
        if ($username != "" && $password != "") {
            //Login - POST /oauth2/token
            $auth_url = $instance_url . "/oauth2/token";

            $oauth2_token_arguments = array(
                "grant_type" => "password",
                //client id - default is sugar.
                //It is recommended to create your own in Admin > OAuth Keys
                "client_id" => "test",
                "client_secret" => "test",
                "username" => $username,
                "password" => $password,
                //platform type - default is base.
                //It is recommend to change the platform to a custom name such as "custom_api" to avoid authentication conflicts.
                "platform" => "base"
            );

            $auth_request = curl_init($auth_url);
            curl_setopt($auth_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
            curl_setopt($auth_request, CURLOPT_HEADER, false);
            curl_setopt($auth_request, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($auth_request, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($auth_request, CURLOPT_FOLLOWLOCATION, 0);
            curl_setopt($auth_request, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));

            //convert arguments to json
            $json_arguments = json_encode($oauth2_token_arguments);
            curl_setopt($auth_request, CURLOPT_POSTFIELDS, $json_arguments);

            //execute request
            $oauth2_token_response = curl_exec($auth_request);

            //decode oauth2 response to get token
            $oauth2_token_response_obj = json_decode($oauth2_token_response);

            return $oauth2_token_response_obj->access_token;
        } else {
            return "";
        }
    }
    ?>
</body>

</html>