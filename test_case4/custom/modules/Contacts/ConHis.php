<?php

require_once 'modules/Contacts/Contact.php';
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class ConHis extends Contact
{
    function passName($bean, $event, $arguments)
    {
        if ($bean->account_id) {
            $bean->fetched_row = $bean->account_id;
        }
    }
    function insertNew($bean, $event, $arguments)
    {
        if (isset($bean->fetched_row) && ($bean->account_id != $bean->fetched_row)) {

            $conHisBeans = BeanFactory::newBean('ConHi_Contact_History');

            // create a new id
            $id = create_guid();
            $conHisBeans->id = $id;

            // set new record flag
            $conHisBeans->new_with_id = true;

            // get account name from id
            $accBeans = BeanFactory::getBean('Accounts', $bean->account_id);
            $accName = $accBeans->name;

            // set contact History information
            $conHisBeans->name = ($accName . " " . date("M,d,Y H:i:s a"));
            $conHisBeans->account_name_c = $accName;
            $conHisBeans->accnt_id_c = $bean->account_id;
            $conHisBeans->assigned_user_id = $bean->assigned_user_id;
            $conHisBeans->c_id = $bean->id;
            $conHisBeans->save();

            // add to relationship
            $bean->load_relationship('conhi_contact_history_contacts');
            $bean->conhi_contact_history_contacts->add($conHisBeans);


            error_log($conHisBeans->name);
            error_log('Contact ID: ' . $bean->id);
            error_log('ConHis ID: ' . $conHisBeans->id);
            error_log('Account ID: ' . $bean->account_id);
        }
    }

    function deleteRelationship($bean, $event, $arguments)
    {
        $tbDeleted = BeanFactory::retrieveBean('ConHi_Contact_History');
        if ($bean->load_relationship('conhi_contact_history_contacts')) {
            $relatedBeans = $bean->conhi_contact_history_contacts->get();
        } else {
            $relatedBeans = null;
        }

        foreach ($relatedBeans as $relatedBean) {
            $tempBean = BeanFactory::retrieveBean('ConHi_Contact_History', $relatedBean);
            error_log("TempBean: " . $tempBean->c_id . "ContactBean" . $bean->id);
            if ($tempBean->c_id == $bean->id) {
                error_log("Deleting... "  . $tempBean->account_name_c);
                //Set deleted to true
                $tbDeleted->mark_deleted($relatedBean);

                //Save
                $tbDeleted->save();
            }
        }
    }
}
function insertNew($bean, $event, $arguments)
{
    if (isset($bean->fetched_row) && ($bean->account_id != $bean->fetched_row)) {
        $url = "http://dev1.oaprojects.ca/service/v4_1/rest.php";
        $username = "admin";
        $password = "Welcome2019!";
        $login_parameters = array(
            "user_auth" => array(
                "user_name" => $username,
                "password" => md5($password),
                "version" => "1"
            ),
            "application_name" => "insertNewConHis",
            "name_value_list" => array(),
        );

        $login_result = call("login", $login_parameters, $url);


        //get session id
        $session_id = $login_result->id;

        //create account -------------------------------------     
        $set_entry_parameters = array(
            //session id
            "session" => $session_id,

            //The name of the module from which to retrieve records.
            "module_name" => "ConHi_Contact_History",

            //Record attributes
            "name_value_list" => array(
                //to update a record, you will need to pass in a record id as commented below
                array("name" => "name", "value" => $bean->name . " " . $bean->account_name),
                array("name" => "c_id", "value" => $bean->id),
            ),
        );

        $set_entry_result = call("set_entry", $set_entry_parameters, $url);

        error_log('New: ' . $bean->account_id);

        // $bean->load_relationship('conhi_contact_history_contacts');
        // $conHisBeans = BeanFactory::retrieveBean('ConHi_Contact_History');
        // error_log($conHisBeans->id);
        // if ($conHisBeans->name == ($bean->name . " " . $bean->account_name)) {
        //     $bean->conhi_contact_history_contacts->add(BeanFactory::retrieveBean('ConHi_Contact_History', $conHisBeans->id));
        // }
    }
}
function call($method, $parameters, $url)
{
    ob_start();
    $curl_request = curl_init();

    curl_setopt($curl_request, CURLOPT_URL, $url);
    curl_setopt($curl_request, CURLOPT_POST, 1);
    curl_setopt($curl_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
    curl_setopt($curl_request, CURLOPT_HEADER, 1);
    curl_setopt($curl_request, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curl_request, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl_request, CURLOPT_FOLLOWLOCATION, 0);

    $jsonEncodedData = json_encode($parameters);

    $post = array(
        "method" => $method,
        "input_type" => "JSON",
        "response_type" => "JSON",
        "rest_data" => $jsonEncodedData
    );

    curl_setopt($curl_request, CURLOPT_POSTFIELDS, $post);
    $result = curl_exec($curl_request);
    curl_close($curl_request);

    $result = explode("\r\n\r\n", $result, 2);
    $response = json_decode($result[1]);
    ob_end_flush();

    return $response;
}
