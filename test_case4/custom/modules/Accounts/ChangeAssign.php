<?php
// METHOD 1 (Passable)
// require_once 'modules/Accounts/Account.php';
// if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
// class ChangeAssign extends Account
// {
//     function changeOnSave($bean, $event, $arguments)
//     {
//         if ($bean->account_type == 'Analyst') {
//              $bean->assigned_user_id = '7eea33e0-714e-11e9-aca9-bc764e1096c0';
//              error_log($bean->assigned_user_name);
//         } else if ($bean->account_type == 'Competitor') {
//             $bean->assigned_user_id = '5d5c96be-714e-11e9-847b-bc764e1096c0';
//         }
//     }
// }

// METHOD 2 (WIP)
require_once 'modules/Accounts/Account.php';
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class ChangeAssign extends Account
{
    function changeOnSave($bean, $event, $arguments)
    {
        // error_log('bleh');

        $sql = new SugarQuery();
        if ($bean->load_relationship('assigned_user_link')) {
            // error_log('Milestone_1');
            $relatedBeans  = $bean->assigned_user_link->getBeans();

            // error_log('Milestone_1.2');
            $sql->select('id');
            $sql->from($relatedBeans);

            if ($bean->account_type == 'Analyst') {
                // error_log('Milestone_2.1');
                // error_log($bean->assigned_user_id);

                $sql->where()->equals('user_name', 'jane.smith');
                $bean->assigned_user_id = $sql->execute();
            } else if ($bean->account_type == 'Competitor') {
                // error_log('Milestone_2.2');
                error_log($bean->assigned_user_id);

                $sql->where()->equals('user_name', 'john.doe');
                $bean->assigned_user_id = $sql->execute();
            }
        }
    }
}
