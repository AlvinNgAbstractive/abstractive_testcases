<?php
$hook_version = 1;
$hook_array = array();

$hook_array['before_save'] = array();
$hook_array['before_save'][] = array(
    // Proccessing Index 
    1,

    // Description
    'test_case4.1, Assigns the record to Jane Smith if Account Type is analyst and \n
     Assigns the record to John Doe if the Account Type is Competitor ',

    // Path to class
    'custom/modules/Accounts/ChangeAssign.php',

    //Class Name
    'ChangeAssign',

    // Method Name
    'changeOnSave'
);
