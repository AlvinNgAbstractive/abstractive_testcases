<?php
$hook_version = 1;
$hook_array = Array();

$hook_array['after_retrieve'] = array();
$hook_array['after_retrieve'][] = array(
    // Proccessing Index 
    1,

    // Description
    'test_case4.2 retrieves the current name and passes it to after_save',

    // Path to class
    'custom/modules/Contacts/ConHis.php',

    //Class Name
    'ConHis',

    // Method Name
    'passName'
);

$hook_array['after_save'] = array();
$hook_array['after_save'][] = array(
    // Proccessing Index 
    2,

    // Description
    'test_case4.2, insert/create a new Contact History record which is going to be linked to the contact record',

    // Path to class
    'custom/modules/Contacts/ConHis.php',

    //Class Name
    'ConHis',

    // Method Name
    'insertNew'
);

$hook_array['before_delete'] = array();
$hook_array['before_delete'][] = array(
    // Proccessing Index 
    3,

    // Description
    'test_case4.2, when the Contac  t record gets deleted, delete all the related Contact History records',

    // Path to class
    'custom/modules/Contacts/ConHis.php',

    //Class Name
    'ConHis',

    // Method Name
    'deleteRelationship'
);
