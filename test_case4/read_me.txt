Requirements
4.1
- using logic hooks make it so that before save change assigned to value when the user saves the account records so it will be the Accounts module logic hook.
    - Account Record Type== Analyst then Assign the record to Jane Smith
    - Account Record Type==Competitor then Assign the record to John Doe

4.2
- Create custom Module using module builder
    - module type: basic
    - module name: Contact History
    - add relationship: Contacts (1) - Contact History (Many)
    - Reference Doc https://support.sugarcrm.com/Documentation/Sugar_Versions/8.0/Pro/Administration_Guide/Developer_Tools/Module_Builder/

- Deploy the custom module and create logic hooks
    1) logic hook: whenever contact's module record Account relationship value (Account Name) change and save, 
        insert/create a new Contact History record which is going to be linked to the contact record 
            - (things to think about: You need to think about when are you going to create the Contact History record;
               when account-contact relationship added(after or before) or deleted(after or before)
        - name format for the Contact History record: <Related Contact's Account Name> - <Relaitonship Added Date/Time>
    2) logic hook: when the Contact record gets deleted, delete all the related Contact History records with logic hook