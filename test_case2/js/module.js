$(document).ready(function () {
    $('.accLink').click(function () {
        console.log('accounts');
        $.ajax({
            type: "POST",
            url: 'table.php',
            data: {
                "module": "Accounts"
            },
            success: function () {
                window.location.href = 'table.php';
            }
        });
    })
    $('.contactLink').click(function () {
        console.log('contactLink');
        $.ajax({
            type: "POST",
            url: 'table.php',
            data: {
                "module": "Contacts"
            },
            success: function () {
                window.location.href = 'table.php';
            }
        });
    })
    $('.leadsLink').click(function () {
        console.log('leadsLink');
        $.ajax({
            type: "POST",
            url: 'table.php',
            data: {
                "module": "Leads"
            },
            success: function () {
                window.location.href = 'table.php';
            }
        });
    })
    $('.oppLink').click(function () {
        console.log('oppLink');
        $.ajax({
            type: "POST",
            url: 'table.php',
            data: {
                "module": "Opportunities"
            },
            success: function () {
                window.location.href = 'table.php';
            }
        });
    })
});