$(document).ready(function () {
    // Clicking login sends username, password
    $('#login').click(function () {
        console.log("bleh you logged in");
        $.ajax({
            type: "POST",
            url: 'index.php',
            data: {
                "username": $("#username").val(),
                "password": $("#password").val(),
            },
            success: function (success) {
                if (success.includes('success')) {
                    window.location.href = 'module.php';
                }
                else {
                    alert("Not a valid username or password.")
                }
            },
        });
    });
});