var offset_count = 0;
$(document).ready(function () {
    // hide back button to begin with
    $('#back').hide();
    const urlParams = new URLSearchParams(window.location.search);
    var module = urlParams.get('module');
    var total = parseInt($('#totalEntries').text(), 10);

    if (total <= 10) {
        $('#back').hide();
        $('#next').hide();
    }
    $('#counter').text('page 1/' + Math.ceil((total / 10)));

    // if click next reloads the table with next 10 
    $('#next').click(function () {
        $('#display_table').load('table.php?module=' + module + ' #display_table', {
            'offset': offset_count += 10
        });
        return false;
    });

    // if click back reloads the table with last 10 
    $('#back').click(function () {
        $('#display_table').load('table.php?module=' + module + ' #display_table', {
            'offset': offset_count -= 10
        });
        return false;
    });

    // on next or back do this
    $('.backForth').click(function () {
        $('#counter').text('page ' + (offset_count / 10 + 1) + '/' + Math.ceil((total / 10)));
        if (offset_count >= (total - 10)) {
            $('#next').hide();
        } else if (offset_count < (total - 10)) {
            $('#next').show();
        }

        if (offset_count >= 10) {
            $('#back').show();
        } else if (offset_count < 10) {
            $('#back').hide();
        }

    });

    // Clicking allows user to change the download csv on the server 
    $('#getCsv').click(function () {
        console.log("bleh");
        $.ajax({
            type: "POST",
            url: 'table.php',
            data: {
                "csv": "clicked"
            },
            success: function (csv) {
                window.location.href = 'download/download.csv';
            }
        });
    });

    // Clicking allows user to change the download csv on the server 
    $('#getpdf').click(function () {
        console.log("bleh2");
        $.ajax({
            type: "POST",
            url: 'table.php',
            data: {
                "pdf": "clicked"
            },
            success: function (pdf) {
                window.location.href = 'http://api.pdflayer.com/api/convert?access_key=1363f118acff2e3eab7eb973d6732ac8&document_url=http://alvinprojects.com/abstractive/test_case2/download/download.html';
            }
        });
    });

    // on enter click search button
    // $("#searchInput").keyup(function (event) {
    //     if (event.keyCode === 13) {
    //         $("#searchBtn").click();
    //     }
    // });
    $("#searchInput").keyup(function (event) {
        if (event) {
            search();
        }
    });

    $('#reset').click(function () {
        $('#searchInput').val('');
        search();
    });

    function search() {
        if ($('#searchInput').val() != "") {
            var searchVal = $('#searchInput').val();
        } else {
            var searchVal = '';
        }
        if ($('#searchField').val() != "") {
            var searchField = $('#searchField').val();
        }
        $('#display_table').load('table.php #display_table', {
            'module': module,
            'search': searchVal,
            'searchField': searchField,
        });
        console.log(module);
        console.log($('#searchInput').val());
        console.log($('#searchField').val());
    }
    // user search box
    // $('#searchBtn').click(function () {
    //     if ($('#searchInput').val() != "") {
    //         var searchVal = $('#searchInput').val();
    //     } else {
    //         var searchVal = '';
    //     }
    //     if ($('#searchField').val() != "") {
    //         var searchField = $('#searchField').val();
    //     }
    //     $('#display_table').load('table.php #display_table', {
    //         'module': module,
    //         'search': searchVal,
    //         'searchField': searchField,
    //     });
    //     console.log(module);
    //     console.log($('#searchInput').val());
    //     console.log($('#searchField').val());
    // });



});