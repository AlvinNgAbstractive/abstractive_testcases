<!DOCTYPE html>
<html lang="en">
<?php
session_start();
?>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/skeleton.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="js/module.js"></script>
    <title>Where to go</title>
</head>

<body>
    <?php
    // echo $_SESSION['user'];
    // echo $_SESSION['pass'];
    // echo $_SESSION['token'];
    ?>
    <h1>Select a module</h1>
    <ul>
        <li id="accLink"><a href="table.php?module=Accounts">Accounts</a></li>
        <li id="contactLink"><a href="table.php?module=Contacts">Contacts</a></li>
        <li id="leadsLink"><a href="table.php?module=Leads">Leads</a></li>
        <li id="oppLink"><a href="table.php?module=Opportunities">Opportunities</a></li>
    </ul>
</body>

</html>