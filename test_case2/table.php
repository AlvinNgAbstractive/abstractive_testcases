<!DOCTYPE html>
<html lang="en">

<?php
session_start();
// error_reporting(0);
// initial 
if (isset($_GET['module']) && ($_GET['module'] == 'Opportunities' || $_GET['module'] == 'Accounts' || $_GET['module'] == 'Contacts' || $_GET['module'] == 'Leads')) {
    $module = $_GET['module'];
}
// for search
else if (isset($_POST['module']) && ($_POST['module'] == 'Opportunities' || $_POST['module'] == 'Accounts' || $_POST['module'] == 'Contacts' || $_POST['module'] == 'Leads')) {
    $module = $_POST['module'];
} else {
    $module = $_SESSION['prevModule'];
}
?>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/skeleton.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="js/table.min.js"></script>
    <title><?php echo $module ?></title>
</head>

<body>
    <header><a href="module.php">Back to Modules</a></header>
    <span class="row" id="row1">
        <span>
            <input type="text" id="searchInput" placeholder="Search">
            <!-- <button id="searchBtn">Search</button> -->
            <button id="reset">reset</button>
        </span><span>
            <button id="getCsv">Download CSV</button>
            <button id="getpdf">Download PDF</button>
        </span>
    </span>
    <?php if (isset($_POST['search'])) {
        $searchInput = $_POST['search'];
    } else {
        $searchInput = '';
    }
    if (isset($_POST['offset'])) {
        $offset = $_POST['offset'];
    } else {
        $offset = 0;
    }

    if (isset($_POST['searchField'])) {
        $searchField = $_POST['searchField'];
    } else {
        $searchField = 'id';
    }
    $instance_url = $_SESSION['iUrl'];
    $username = $_SESSION['user'];
    $password = $_SESSION['pass'];

    $oauth_token = $_SESSION['token'];

    //Identify records to export - POST /<module>/filter
    $filter_url = $instance_url . "/" . $module . "/filter";

    if (isset($_SESSION['content']) && ($_SESSION['prevModule'] == $module) && ($searchInput == $_SESSION['prevSearch']) && ($_SESSION['content']->records != null)) {
        // echo ($_SESSION['content']->records[1]->id);
        $filter_arguments = $_SESSION['arguments'];
        $filter_response_obj = $_SESSION['content'];
    } else {
        // $searchInput = '';
        $filter_arguments = array(
            "filter" => array(
                array(
                    '$or' => array(
                        array($searchField => array('$contains' =>  $searchInput)),
                    ),
                ),
            ),
            "max_num" => 1000000,
            "fields" => [
                'name', 'account_type', "phone_office", "billing_address_street", "billing_address_city",
                "billing_address_state", "billing_address_postalcode", "billing_address_country"
            ],
            "order_by" => "date_entered",
            "favorites" => false,
            "my_items" => false,
        );

        $filter_arguments = moduleToFields($filter_arguments, $module, $searchInput, $searchField);
        $filter_response_obj = getFilterResponse($filter_url, $oauth_token, $filter_arguments);
        $_SESSION['arguments'] = $filter_arguments;
        $_SESSION['content'] = $filter_response_obj;
        $_SESSION['prevSearch'] = $searchInput;
    }

    echo '<span class="row">';
    echo '<label for="searchField">Search Field</label>';
    echo '<select id="searchField">';
    printOptions($module);
    echo '</select>';
    echo '</span>';
    echo makeTable($module, $filter_response_obj, $offset);

    // $record_list_response_obj = getRecords($oauth_token, $filter_response_obj, $instance_url, $module);

    $fp = fopen('download/download.csv', 'w');
    echo $module;
    fwrite($fp, createFilteredCsv($filter_response_obj, $module));
    fclose($fp);

    if (isset($_POST['pdf'])) {
        file_put_contents('download/download.html', "");
        $fp = fopen('download/download.html', 'w');
        fwrite($fp, '<!DOCTYPE html> <html lang="en">');
        fwrite($fp, '<head>');
        fwrite($fp, '    <meta charset="UTF-8">');
        fwrite($fp, '    <meta name="viewport" content="width=device-width, initial-scale=1.0">');
        fwrite($fp, '    <meta http-equiv="X-UA-Compatible" content="ie=edge">');
        fwrite($fp, '    <title>Document</title>');
        fwrite($fp, '</head>');
        fwrite($fp, '<body>');
        fwrite($fp, makeTable($module, $filter_response_obj, $offset));
        fwrite($fp, '</body>');
        fwrite($fp, '</html>');
        fclose($fp);
    }
    if (isset($_POST['csv'])) {
        $fp = fopen('download/download.csv', 'w');
        fwrite($fp, createFilteredCsv($filter_response_obj, $module));
        fclose($fp);
    }
    if (($_SESSION['token'] == '') && ($searchInput == '')) {
        header("Location: index.php");
    }
    $_SESSION['prevModule'] = $module;
    // file_put_contents('download/download.html', "");
    ?> <div class="row">
        <span class="six columns">Total Entries: <span id="totalEntries"><?php echo getTotal($instance_url, $oauth_token, $filter_arguments, $module); ?></span></span>
        <span class="six columns" id="page_control">
            <span id="counter"></span>
            <button class="backForth" id="back">back</button>
            <button class="backForth" id="next">next</button>
        </span>
    </div>
</body>

</html>
<?php
function printOptions($module)
{
    if ($module == 'Accounts') {
        echo '    <option value="name">Name</option>';
        echo '    <option value="phone_office">Phone #</option>';
        echo '    <option value="billing_address_street">Street</option>';
        echo '    <option value="billing_address_city">City</option>';
        echo '    <option value="billing_address_state">State</option>';
        echo '    <option value="billing_address_postalcode">Postal Code</option>';
        echo '    <option value="billing_address_country">Country</option>';
        echo '    <option value="account_type">Type</option>';
    } else if (($module == 'Contacts') || ($module == 'Leads')) {
        echo '    <option value="first_name">First Name</option>';
        echo '    <option value="last_name">Last Name</option>';
        echo '    <option value="title">Title</option>';
        // echo '    <option value="phone_work">Office phone</option>';
        // echo '    <option value="phone_mobile">Mobile</option>';
        // echo '    <option value="phone_home">Home phone</option>';
        echo '    <option value="lead_source">Source</option>';
    } else if ($module == 'Opportunities') {
        echo '    <option value="name">Name</option>';
        echo '    <option value="sales_status">Status</option>';
        // echo '    <option value="amount">Amount</option>';
    }
    if ($module == 'Leads') {
        echo '    <option value="status">Status</option>';
    }
}
function getFilterResponse($filter_url, $oauth_token, $filter_arguments)
{
    $filter_request = curl_init($filter_url);
    curl_setopt($filter_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
    curl_setopt($filter_request, CURLOPT_HEADER, false);
    curl_setopt($filter_request, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($filter_request, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($filter_request, CURLOPT_FOLLOWLOCATION, 0);
    curl_setopt($filter_request, CURLOPT_HTTPHEADER, array(
        "Content-Type: application/json",
        "oauth-token: {$oauth_token}"
    ));

    //convert arguments to json
    $json_arguments = json_encode($filter_arguments);
    curl_setopt($filter_request, CURLOPT_POSTFIELDS, $json_arguments);

    //execute request
    $filter_response = curl_exec($filter_request);

    //decode json
    return json_decode($filter_response);
}
function getRecords($oauth_token, $filter_response_obj, $instance_url, $module)
{
    //store ids of records to export
    $export_ids = array();
    foreach ($filter_response_obj->records as $record) {
        $export_ids[] = $record->id;
    }
    //Create a record list - POST /<module>/record_list
    $record_list_url = $instance_url . "/" . $module . "/record_list";

    $record_list_arguments = array(
        "records" => $export_ids,
    );

    $record_list_request = curl_init($record_list_url);
    curl_setopt($record_list_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
    curl_setopt($record_list_request, CURLOPT_HEADER, false);
    curl_setopt($record_list_request, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($record_list_request, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($record_list_request, CURLOPT_FOLLOWLOCATION, 0);
    curl_setopt($record_list_request, CURLOPT_HTTPHEADER, array(
        "Content-Type: application/json",
        "oauth-token: {$oauth_token}"
    ));

    //convert arguments to json
    $json_arguments = json_encode($record_list_arguments);
    curl_setopt($record_list_request, CURLOPT_POSTFIELDS, $json_arguments);

    //execute request
    $record_list_response = curl_exec($record_list_request);
    $record_list_response_obj = json_decode($record_list_response);
    return $record_list_response_obj;
}
// changes the fields based on the module
function moduleToFields($filter_arguments, $module, $searchInput, $searchField)
{
    if ($module == 'Contacts' || $module == 'Leads') {
        $filter_arguments['fields'] =  [
            'first_name', 'last_name', 'phone_work', 'phone_mobile', 'phone_home',
            'status', 'lead_source', 'title'
        ];
        $filter_arguments['filter'] = array(
            array(
                '$or' => array(
                    array($searchField => array('$contains' =>  $searchInput))
                ),
            ),
        );
    } else if ($module == 'Opportunities') {
        $filter_arguments['fields'] = [
            'name', 'sales_status', 'amount', 'best_case',
            'worst_case'
        ];
        $filter_arguments['filter'] = array(
            array(
                '$or' => array(
                    array($searchField => array('$contains' =>  $searchInput)),
                ),
            ),
        );
    }
    return $filter_arguments;
}
// Creates and populates the tables based on the module name
function makeTable($module, $filter_response_obj, $offset)
{
    $content = '<table id="display_table">';
    // Table elements for account 
    if ($module == 'Accounts') {
        $content .= '    <tr>';
        $content .= '        <td>ID</td>';
        $content .= '        <td>Name</td>';
        $content .= '        <td>Phone</td>';
        $content .= '        <td>Street</td>';
        $content .= '        <td>City</td>';
        $content .= '        <td>State</td>';
        $content .= '        <td>Postal code</td>';
        $content .= '        <td>Country</td>';
        $content .= '        <td>Type</td>';
        $content .= '    </tr>';

        for ($i = $offset; $i < $offset + 10; $i++) {
            if ($filter_response_obj->records[$i] != null) {
                $value = $filter_response_obj->records[$i];
                $content .= "<tr>";
                $content .= "<td>" . $value->id . "</td>";
                $content .= "<td>" . $value->name . "</td>";
                $content .= "<td>" . $value->phone_office . "</td>";
                $content .= "<td>" . $value->billing_address_street . "</td>";
                $content .= "<td>" . $value->billing_address_city . "</td>";
                $content .= "<td>" . $value->billing_address_state . "</td>";
                $content .= "<td>" . $value->billing_address_postalcode . "</td>";
                $content .= "<td>" . $value->billing_address_country . "</td>";
                $content .= "<td>" . $value->account_type . "<td/>";
                $content .= "</tr>";
            }
        }
    }
    // Table elements for Contacts
    else if ($module == 'Contacts') {
        $content .= '    <tr>';
        $content .= '        <td>ID</td>';
        $content .= '        <td>Name</td>';
        $content .= '        <td>Title</td>';
        $content .= '        <td>Office num</td>';
        $content .= '        <td>Mobile num</td>';
        $content .= '        <td>Home num</td>';
        $content .= '        <td>Status</td>';
        $content .= '        <td>Source</td>';
        $content .= '    </tr>';

        for ($i = $offset; $i < $offset + 10; $i++) {
            if ($filter_response_obj->records[$i] != null) {
                $value = $filter_response_obj->records[$i];
                $content .= "<tr>";
                $content .= "<td>" . $value->id . "</td>";
                $content .= "<td>" . $value->first_name . " " . $value->last_name . "</td>";
                $content .= "<td>" . $value->title . "</td>";
                $content .= "<td>" . $value->phone_work . "</td>";
                $content .= "<td>" . $value->phone_mobile . "</td>";
                $content .= "<td>" . $value->phone_home . "</td>";
                $content .= "<td>" . $value->lead_source . "</td>";
                $content .= "</tr>";
            }
        }
    }
    // Table elements for Leads 
    else if ($module == 'Leads') {
        $content .= '    <tr>';
        $content .= '        <td>ID</td>';
        $content .= '        <td>Name</td>';
        $content .= '        <td>Title</td>';
        $content .= '        <td>Office num</td>';
        $content .= '        <td>Mobile num</td>';
        $content .= '        <td>Home num</td>';
        $content .= '        <td>Status</td>';
        $content .= '        <td>Source</td>';
        $content .= '    </tr>';

        for ($i = $offset; $i < $offset + 10; $i++) {
            if ($filter_response_obj->records[$i] != null) {
                $value = $filter_response_obj->records[$i];
                $content .= "<tr>";
                $content .= "<td>" . $value->id . "</td>";
                $content .= "<td>" . $value->first_name . " " . $value->last_name . "</td>";
                $content .= "<td>" . $value->title . "</td>";
                $content .= "<td>" . $value->phone_work . "</td>";
                $content .= "<td>" . $value->phone_mobile . "</td>";
                $content .= "<td>" . $value->phone_home . "</td>";
                $content .= "<td>" . $value->status . "</td>";
                $content .= "<td>" . $value->lead_source . "</td>";
                $content .= "</tr>";
            }
        }
    }
    // Table elements for Opportunities 
    else if ($module == 'Opportunities') {
        $content .= '    <tr>';
        $content .= '        <td>ID</td>';
        $content .= '        <td>Name</td>';
        $content .= '        <td>Stage</td>';
        $content .= '        <td>Amount</td>';
        $content .= '        <td>Best</td>';
        $content .= '        <td>Worst</td>';
        $content .= '    </tr>';

        for ($i = $offset; $i < $offset + 10; $i++) {
            if ($filter_response_obj->records[$i] != null) {
                $value = $filter_response_obj->records[$i];
                $content .= "<tr>";
                $content .= "<td>" . $value->id . "</td>";
                $content .= "<td>" . $value->name . "</td>";
                $content .= "<td>" . $value->sales_status . "</td>";
                $content .= "<td>$" . $value->amount . "</td>";
                $content .= "<td>$" . $value->best_case . "</td>";
                $content .= "<td>$" . $value->worst_case . "</td>";
                $content .= "</tr>";
            }
        }
    }

    $content .= '</table>';
    return $content;
}

// returns the total number of rows to be displayed at the bottom
function getTotal($instance_url, $oauth_token, $filter_for_total, $module)
{
    $filter_url = $instance_url . "/" . $module . "/filter";
    $filter_request = curl_init($filter_url);
    curl_setopt($filter_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
    curl_setopt($filter_request, CURLOPT_HEADER, false);
    curl_setopt($filter_request, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($filter_request, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($filter_request, CURLOPT_FOLLOWLOCATION, 0);
    curl_setopt($filter_request, CURLOPT_HTTPHEADER, array(
        "Content-Type: application/json",
        "oauth-token: {$oauth_token}"
    ));

    //convert arguments to json
    $json_arguments = json_encode($filter_for_total);
    curl_setopt($filter_request, CURLOPT_POSTFIELDS, $json_arguments);

    //execute request
    $filter_response = curl_exec($filter_request);

    //decode json
    $filter_response_obj = json_decode($filter_response);

    $count = 0;
    // THIS IS VERY TIME INEFFICIENT
    foreach ($filter_response_obj->records as $key => $value) {
        if ($value->id != "") {
            $count++;
        }
    }
    return $count;
}

// Creates a csv file from the response objects with the fields given NOT USED
function createFilteredCsv($filter_response_obj, $module)
{
    if ($module == 'Accounts') {
        $csv = '"ID","Name","Phone","Street","City","State","Postal code","Country","Type"' . "\n";
        foreach ($filter_response_obj->records as $key => $value) {
            $csv = $csv . '"' . $value->id . '",';
            $csv = $csv . '"' . $value->name . '",';
            $csv = $csv . '"' . $value->phone_office . '",';
            $csv = $csv . '"' . $value->billing_address_street . '",';
            $csv = $csv . '"' . $value->billing_address_city . '",';
            $csv = $csv . '"' . $value->billing_address_state . '",';
            $csv = $csv . '"' . $value->billing_address_postalcode . '",';
            $csv = $csv . '"' . $value->billing_address_country . '",';
            $csv = $csv . '"' . $value->account_type . '"' . "\n";
        }
    } else if ($module == 'Contacts' || $module == 'Leads') {
        if ($module == 'Contacts') {
            $csv = '"ID","Name","Title","Office Num","Mobile Num","Home Num","Source"' . "\n";
        } else {
            $csv = '"ID","Name","Title","Office Num","Mobile Num","Home Num","Source","Status"' . "\n";
        }
        foreach ($filter_response_obj->records as $key => $value) {
            $csv .= '"' . $value->id . '",';
            $csv .= '"' . $value->first_name . " " . $value->last_name . '",';
            $csv .= '"' . $value->title . '",';
            $csv .= '"' . $value->phone_work . '",';
            $csv .= '"' . $value->phone_mobile . '",';
            $csv .= '"' . $value->phone_home . '",';
            if ($module == 'Leads') {
                $csv .= '"' . $value->lead_source . '",';
                $csv .= '"' . $value->status . '"' . "\n";
            } else {
                $csv .= '"' . $value->lead_source . '"' . "\n";
            }
        }
    } else if ($module == 'Opportunities') {
        $csv = '"ID","Name","Stage","Amount","Best","Worst"' . "\n";
        foreach ($filter_response_obj->records as $key => $value) {
            $csv .= '"' . $value->id . '",';
            $csv .= '"' . $value->name . '",';
            $csv .= '"' . $value->sales_status  . '",';
            $csv .= '"' . $value->amount . '",';
            $csv .= '"' . $value->best_case . '",';
            $csv .= '"' . $value->worst_case . '"' . "\n";
        }
    }

    return $csv;
}


// Creates a csv file from the response objects with the all the fields
function createAllCsv($instance_url, $record_list_response_obj, $oauth_token, $module)
{
    //Export Records - GET /<module>/export/:record_list_id

    $export_url = $instance_url . "/" . $module . "/export/" . $record_list_response_obj->id;

    $export_request = curl_init($export_url);
    curl_setopt($export_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
    curl_setopt($export_request, CURLOPT_HEADER, true); //needed to return file headers
    curl_setopt($export_request, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($export_request, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($export_request, CURLOPT_FOLLOWLOCATION, 0);
    curl_setopt($export_request, CURLOPT_HTTPHEADER, array(
        "Content-Type: application/json",
        "oauth-token: {$oauth_token}"
    ));

    $export_response = curl_exec($export_request);

    //set headers from response
    list($headers, $content) = explode("\r\n\r\n", $export_response, 2);
    foreach (explode("\r\n", $headers) as $header) {
        header($header);
    }

    $content = trim($content);
    return $content;
}
?>